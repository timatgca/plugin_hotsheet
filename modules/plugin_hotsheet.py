# -*- coding: utf-8 -*-

from collections import defaultdict
import re
import random
from decimal import Decimal

import gluon.sqlhtml
from gluon.storage import Storage
from gluon.html import URL, P, A, IMG, XML, DIV, SPAN, TAG, \
                       TABLE, THEAD, TBODY, TH, TR, TD, UL, LI, \
                       FORM, FIELDSET, LABEL, INPUT, TEXTAREA, SELECT, OPTION, \
                       SCRIPT, CAT, xmlescape
from gluon.dal import DAL
from gluon import current

######################### RWSheet ############################
##############################################################

class HOTSheet(FORM):

    def __init__(self, 
                 query,
                 row_field, col_field, data_field,
                 row_ids=None, col_ids=None,
                 row_headers=None, col_headers=None,
                 row_totals=False, col_totals=False,
                 extra_values=None,
                 **attributes):
        '''Muestra una hoja de calculo para editar una relacion que representa
        una funcion (ref1,ref2) -> valor
        '''
        FORM.__init__(self, **attributes)
        self.attributes = attributes
        if not (row_field.table == col_field.table == data_field.table):
            raise AttributeError, "Only one table per HOTSheet"
        self.table = row_field.table
        self.db    = row_field.db
        self.query = query or table
        self._id_field  = self.table.id
        self.row_field  = row_field
        self.col_field  = col_field
        self.data_field = data_field
        if row_totals:
            raise NotImplementedError
        self.col_totals = col_totals
        self.extra_values = extra_values or {}

        #En HOTSheet, row_ids es necesariamente una lista de enteros
        if row_ids and not row_field.type.startswith('reference'):
            raise AttributeError('row_ids specified for row field not of reference type')
        if row_ids==None:
            row_ids   = [r[row_field] for r in self.db(query).select(row_field, groupby=row_field)]
        self.row_ids = row_ids
        self.row_headers = row_headers or row_ids

        if col_ids==None:
            col_ids   = [r[col_field] for r in self.db(query).select(col_field, groupby=col_field)]
        self.col_ids = col_ids
        self.col_headers = col_headers or col_ids

        data = dict((rid, {}) for rid in self.row_ids)
        for r in self.db(query).select(row_field, col_field, data_field):
            data[r[row_field]][r[col_field]] = r[data_field]
        self.data = data

        self.prepare_components()

    def escape(self, xs, data_text = '"%s"'):
        return '[%s]'%','.join(data_text%xmlescape(x) for x in xs)

    def prepare_components(self):
        if self.col_totals:
            pass

        self.components = [
            DIV(DIV(_id='hot'),_id='hot_parent'),
            INPUT(_type='hidden', _name='override', _id='override'),
            INPUT(_type='submit', _name='submit', _id='submit', _class='btn btn-primary'),
            INPUT(_type='submit',
                  _name='discard',
                  _value=current.T('Descartar cambios'),
                  _class='btn btn-warning'),
            ]

        current.response.files.append(URL('static', 'plugin_hotsheet/handsontable/handsontable.full.js'))
        current.response.files.append(URL('static', 'plugin_hotsheet/handsontable/handsontable.full.css'))
        current.response.files.append(URL('static', 'plugin_hotsheet/handsontable/plugins/bootstrap/handsontable.bootstrap.css'))

    def xml(self):
        newform = FORM(*self.components, **self.attributes)

        d = dict(col_headers = self.escape(self.col_headers),
                 row_headers = self.escape(self.row_headers),
                 row_ids = self.escape(self.row_ids),
                 columns = self.escape(
                    self.col_ids,
                    data_text='{data: "%s", type: "numeric", format: "0.00"}'
                 ),
                 data = ('[%s]'%','.join(
                    '{%s}'%(
                        '"fila":%s,'%rid +
                        ','.join('%s:%s'%(colid,val) for colid,val in self.data[rid].iteritems())
                    ) for rid in self.row_ids))
                )
        hotsheet_script = SCRIPT(current.response.render('plugin_hotsheet.html', d),
                                 _type="text/javascript")
        newform.append(hotsheet_script)
        return DIV.xml(newform)

    def accepts(
        self,
        request_vars,
        ):
        '''
        La funcion no acepta todos los parametros habituales de FORM.accepts porque 
        la validacion es 100% javascript, luego llegados a este punto no puede haber errores
        '''
        db = self.db
        data = self.data
        #Ojo: si han pulsado en discard changes, no escribes nada (y recargas la hoja)
        if request_vars.discard or not request_vars.submit:
            return False

        import json as json_parser
        override = json_parser.loads(request_vars.override)
        q = (reduce(lambda a,b:a&b, 
                    ((self.table[k]==v) for k,v in self.extra_values.iteritems()))
             if self.extra_values else None)
        for k,v in override.iteritems():
            rid, cid = map(long, k.split(','))
            val = Decimal(v or 0)
            if cid in data[rid]:
                qrc = (self.row_field==rid) & (self.col_field==cid)
                if q: qrc &=q
                db(qrc).update(**{self.data_field.name:val})
            else:
                d = {self.row_field.name:rid,
                     self.col_field.name:cid,
                     self.data_field.name:val}
                d.update(self.extra_values)
                self.table.insert(**d)
            data[rid][cid] = val

        return True

